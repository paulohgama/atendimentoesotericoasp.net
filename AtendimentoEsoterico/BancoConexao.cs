﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace AtendimentoEsoterico
{
    class BancoConexao
    {
        private readonly string server;
        private readonly string user;
        private readonly string password;
        private readonly string database;

        /* Pode-se, desde logo, atribuir valores por defeito */

        public BancoConexao(string __server = "bddev51.mysql.dbaas.com.br", string __user = "bddev51",
                        string __password = "dV38t@tsj251", string __database = "bddev51")
        {
            this.server = __server;
            this.user = __user;
            this.password = __password;
            this.database = __database;
        }

        public string Get()
        {
            return "Server=" + this.server + ";Database=" + this.database +
                   ";Uid=" + this.user + ";Pwd=" + this.password;
            /* Output: Server=bddev51.mysql.dbaas.com.br;Database=bddev51;id=bddev51;Pwd=dV38t@tsj251 */
        }
    }
}
