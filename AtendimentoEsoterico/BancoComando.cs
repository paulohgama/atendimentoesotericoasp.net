﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Runtime.InteropServices;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace AtendimentoEsoterico
{
    class BancoComandos : Controller
    {
        private BancoConexao db;
        private readonly MySqlConnection connection;
        private MySqlCommand command;
        private MySqlDataAdapter dAdapter;
        private DataSet dSet;


        public BancoComandos()
        {
            db = new BancoConexao();
            connection = new MySqlConnection(db.Get());
        }
        public DataSet Get(string __query, [Optional] MySqlParameter[] __param)
        {
            this.command = new MySqlCommand(__query, this.connection);

            if (__param != null) { this.command.Parameters.AddRange(__param); }

            this.dAdapter = new MySqlDataAdapter(this.command);
            this.dSet = new DataSet();

            /* input exemplo: 'SELECT * FROM tabela WHERE id = @id'
               output: 'FROM tabela' */
            Regex rgx = new Regex("FROM\\s\\w+");
            MatchCollection matches = rgx.Matches(__query);

            /* output: 'tabela' */
            string table = matches[0].Value.Replace("FROM ", "");

            this.dAdapter.Fill(this.dSet, table);

            return this.dSet;
        }
        public int Set(string __query, [Optional] MySqlParameter[] __param)
        {
            try
            {
                this.command = new MySqlCommand(__query, this.connection);

                if (__param != null) { command.Parameters.AddRange(__param); }

                if (this.connection.State != ConnectionState.Open)
                {
                    this.connection.Open();
                }

                return this.command.ExecuteNonQuery();
                // retorna o número de linhas afectadas
            }
            catch (MySqlException e)
            {
                ViewBag.Erro = e.Message.ToString();
                return 0;
            }

        }




    }
}
