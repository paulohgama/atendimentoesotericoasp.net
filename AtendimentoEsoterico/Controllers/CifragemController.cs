﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AtendimentoEsoterico.Controllers
{
    public class CifragemController : Controller
    {
        // GET: Cifragem
        public ActionResult Index()
        {
            return View();
        }

        public string Atbah(string mensagem)
        {
            string retorno = "";

            for(int i = 0; i < mensagem.Length; i ++)
            {
                retorno = mensagem.Replace(mensagem[i], CifraAtbah(mensagem.Substring(0, 1)));
            }

            return retorno;
        }

        private char CifraAtbah(string letra)
        {
            switch(letra)
            {
                case "A": return 'I';
                case "B": return 'H';
                case "C": return 'G';
                case "D": return 'F';
                case "E": return 'N';
                case "F": return 'D';
                case "G": return 'C';
                case "H": return 'B';
                case "I": return 'A';
                case "J": return 'R';
                case "K": return 'Q';
                case "L": return 'P';
                case "M": return 'O';
                case "N": return 'E';
                case "O": return 'M';
                case "P": return 'L';
                case "Q": return 'K';
                case "R": return 'J';
                case "S": return 'Z';
                case "T": return 'Y';
                case "U": return 'X';
                case "V": return 'W';
                case "W": return 'V';
                case "X": return 'U';
                case "Y": return 'T';
                case "Z": return 'S';
                case "a": return 'i';
                case "b": return 'h';
                case "c": return 'g';
                case "d": return 'f';
                case "e": return 'n';
                case "f": return 'd';
                case "g": return 'c';
                case "h": return 'b';
                case "i": return 'a';
                case "j": return 'r';
                case "k": return 'q';
                case "l": return 'p';
                case "m": return 'o';
                case "n": return 'e';
                case "o": return 'm';
                case "p": return 'l';
                case "q": return 'k';
                case "r": return 'j';
                case "s": return 'z';
                case "t": return 'y';
                case "u": return 'x';
                case "v": return 'w';
                case "w": return 'v';
                case "x": return 'u';
                case "y": return 't';
                case "z": return 's';
            }
            return letra[0];
        }

    }
}