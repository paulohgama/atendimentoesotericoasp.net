﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using AtendimentoEsoterico;
using MySql.Data.MySqlClient;
using System.Web.Script.Serialization;
using System.Web.Routing;

namespace AtendimentoEsoterico.Controllers
{
    public class ConsultorController : Controller
    {
        private BancoComandos bd = new BancoComandos();
        // GET: Consultor
        public ActionResult Index()
        {
            int id = int.Parse(Session["consultor"].ToString());
            string query = "select status FROM aspnet_consultor where id_consultor = "+ id +";";
            string resultado = bd.Get(query, null).Tables[0].Rows[0][0].ToString();
            try { resultado = (bool.Parse(resultado)) ? "1" : "0"; } catch (Exception ex) { Console.Write(ex); }
            id = int.Parse(resultado);
            string[] status = { "Offline", "Online", "Em consulta", "Fora do horario de trabalho" };
            ViewBag.Status = status[id];
            return View();
        }
        public string PegaDados()
        {
            string OrderColumn = null;
            string OrderDir = null;
            string SearchColumn = null;
            string SearchValue = null;
            int Inicio = 0;
            int Tamanho = 0;
            string draw = null;
            try { draw = Request.Params.Get("draw").ToString(); } catch(NullReferenceException ex) { Console.Write(ex); }
            try { OrderColumn = Request.Params.Get("order[0][column]").ToString(); } catch(NullReferenceException ex) { Console.Write(ex); }
            try { OrderDir = Request.Params.Get("order[0][dir]").ToString(); } catch(NullReferenceException ex) { Console.Write(ex); }
            try { SearchColumn = Request.Params.Get("search[column]").ToString(); } catch(NullReferenceException ex) { Console.Write(ex); }
            try { SearchValue = Request.Params.Get("search[value]").ToString(); } catch(NullReferenceException ex) { Console.Write(ex); }
            try { Inicio = int.Parse(Request.Params.Get("start").ToString()); } catch(NullReferenceException ex) { Console.Write(ex); }
            try { Tamanho = int.Parse(Request.Params.Get("length").ToString()); } catch(NullReferenceException ex) { Console.Write(ex); }
            string query = ConstroiQuery(SearchValue);
            DataSet dataSet = ConstroiDataSet(query, SearchValue, OrderColumn, OrderDir, Inicio, Tamanho);
            DataTable dt = dataSet.Tables[0];
            object json = PopulaDados(dt);
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            Dictionary<string, object> childRow = new Dictionary<string, object>
            {
                { "draw", draw },
                { "recordsTotal", ResultadosTotais(SearchValue) },
                { "recordsFiltered", ResultadosFiltrados(query, SearchValue, OrderColumn, OrderDir, Inicio, Tamanho) },
                { "data", json }
            };

            return jsSerializer.Serialize(childRow); ;
        }
        public object PopulaDados(DataTable table)
        {
            object[] result = new object[table.Rows.Count];
            for(int i = 0; i < table.Rows.Count; i++)
            { 
                object[] partial = new object[8];
                partial[0] = table.Rows[i][0].ToString();
                partial[2] = (bool.Parse(table.Rows[i][2].ToString()) ? "Ocupado" : "Disponivel");
                partial[1] = table.Rows[i][1].ToString();
                partial[3] = RetornaSemanas(table.Rows[i][3].ToString());
                partial[4] = table.Rows[i][4].ToString();
                partial[5] = table.Rows[i][5].ToString();
                partial[6] = "<a href='"+ Url.Action("Editar", "Consultor", new RouteValueDictionary(new { id = int.Parse(table.Rows[i][6].ToString()) })) +"' class='btn btn-primary' role='button'>Editar </a> ";
                partial[7] = "<a href='"+ Url.Action("Deletar", "Consultor", new RouteValueDictionary(new { id = int.Parse(table.Rows[i][6].ToString()) })) +"' class='btn btn-danger' role='button'> Deletar </a> ";
                result[i] = partial;
            }
            return result;
        }
        private string ConstroiQuery(string search)
        {
            string query = "SELECT nome, email, status, dias, horarios, descricao, id_consultor FROM aspnet_consultor ";
            if(search != null && search != "")
            {
                query += "where like nome = %@search% ";
            }
            return query;
        }
        private DataSet ConstroiDataSet(string query, string search, string orderC, string orderD, int inicio, int tamanho)
        {
            if(orderC != null && orderD != null)
            {
                query += "order by " + OrderByC(orderC) +" " + OrderByD(orderD) +  " ";
            }
            query += "limit " + tamanho +" offset " + inicio+ ";";
            DataSet data = this.bd.Get(query, null);

            return data;
        }
        private string OrderByD(string orderD)
        {
            return (orderD == null) ? "DESC" : orderD.ToUpper();
        }
        private string OrderByC(string orderC)
        {
            return (orderC == null) ? "id_consultor" : RetornaColuna(int.Parse(orderC));
        }
        private int ResultadosTotais(string search)
        {
            string query = ConstroiQuery(search);
            MySqlParameter[] param = new MySqlParameter[]
            {
                new MySqlParameter("@search", search),
                new MySqlParameter("@orderDir",  (null == null) ? "desc" : ""),
                new MySqlParameter("@orderColumn", (null == null) ? "id_consultor" : ""),
            };
            DataSet data = this.bd.Get(query, param);
            return data.Tables[0].Rows.Count;
        }
        private int ResultadosFiltrados(string query, string search, string orderC, string orderD, int inicio, int tamanho)
        {
            DataSet dataSet = ConstroiDataSet(query, search, orderC, orderD, inicio, tamanho);
            return dataSet.Tables[0].Rows.Count;
        }
        private string RetornaColuna(int coluna)
        {
            switch(coluna)
            {
                case 0: return "nome";
                case 1: return "email";
                case 2: return "status";
                case 3: return "dias";
                case 4: return "horariois";
                case 5: return "descricao";
                default: return "";
            }
        }
        public ActionResult Editar(int id)
        {
            Session.Remove("Titulo");
            Session.Remove("Exibicao");
            Session.Remove("Tipo");
            Session.Add("Titulo", "Editando Consultor");
            Session.Add("Exibicao", "Editando");
            Session.Add("id", id);
            Session.Add("Tipo", "Editar");
            return RedirectToAction("Editar", "Admin");
           
        }
        [HttpPost]
        public ActionResult Editando(int id, HttpPostedFile foto)
        {
            string query;
            string nome = Request.Params.Get("nome");
            string email = Request.Params.Get("email");
            string descricao = Request.Params.Get("desc");
            string horario1 = Request.Params.Get("hora1");
            string horario2 = Request.Params.Get("hora2");
            AdminController admin = new AdminController();
            string dia = admin.RetornaDias(Request.Params.GetValues("dias"));
            MySqlParameter[] param = new MySqlParameter[]
            {
                new MySqlParameter("@nome", nome),
                new MySqlParameter("@email", email),
                new MySqlParameter("@descricao", descricao),
                new MySqlParameter("@horarios", (horario1 != "" && horario2 != "")
                                                    ? horario1 + " até " + horario2
                                                    : "A definir"),
                new MySqlParameter("@dias", dia),
                new MySqlParameter("@id", id)
            };
            int ide = id;
            query = "update aspnet_consultor set nome = @nome, email = @email, descricao = @descricao, horarios = @horarios, dias = @dias where id_consultor = @id";
            int linhasAfetadas = bd.Set(query, param);
            if (linhasAfetadas > 0)
            {
                ViewBag.Error = "Atualização efetuada!";
                return RedirectToAction("Editar", "Consultor", new RouteValueDictionary(new { id = ide }));
            }
            else
            {
                ViewBag.Error = "Atualização efetuada";
            }
            return RedirectToAction("Editar", "Consultor", new RouteValueDictionary(new { id = ide }));
        }
        public ActionResult Deletar(int id)
        {
            string query = "delete from aspnet_consultor where id_consultor = @id";
            if (id > 0)
            {
                MySqlParameter[] param = new MySqlParameter[]
            {
                new MySqlParameter("@id", id)
            };
                int linhasAfetadas = 0;
                try { linhasAfetadas = bd.Set(query, param); } catch (MySqlException ex) { Console.Write(ex); }
                if (linhasAfetadas == 0)
                {
                    ViewData["Error"] = "Não foi possivel deletar o consultor";
                    return RedirectToAction("Consultores", "Admin");
                }
                else
                {
                    ViewData["Error"] = "Consultor deletado com sucesso";
                    return RedirectToAction("Consultores", "Admin");
                }
            }
            ViewData["Error"] = "Consultor não identificado";
            return RedirectToAction("Consultores", "Admin");
        }
        private string RetornaSemanas(string dias)
        {
            string retorna = "";
            string[] dia = dias.Split(',');
            if(dia[0].Equals("A definir"))
            {
                return "A definir";
            }
            for(int i = 0; i < dia.Length; i++)
            {
               switch(int.Parse(dia[i].Trim()))
                {
                    case 1: if(i != dia.Length-1) retorna += "Domingo, "; else retorna = retorna.Substring(0, retorna.Length - 2) + " e Domingo"; break;
                    case 2: if(i != dia.Length-1) retorna += "Segunda, "; else retorna = retorna.Substring(0, retorna.Length - 2) + " e Segunda"; break;
                    case 3: if(i != dia.Length-1) retorna += "Terça, ";  else retorna = retorna.Substring(0, retorna.Length - 2) + " e Terça"; break;
                    case 4: if(i != dia.Length-1) retorna += "Quarta, ";  else retorna = retorna.Substring(0, retorna.Length - 2) + " e Quarta"; break;
                    case 5: if(i != dia.Length-1) retorna += "Quinta, ";  else retorna = retorna.Substring(0, retorna.Length - 2) + " e Quinta"; break;
                    case 6: if(i != dia.Length-1) retorna += "Sexta, ";  else retorna = retorna.Substring(0, retorna.Length - 2) + " e Sexta"; break;
                    case 7: if(i != dia.Length-1) retorna += "Sabado, ";  else retorna = retorna.Substring(0, retorna.Length - 2) + " e Sabado"; break;
                    default: retorna += "";
                    break;
                }
            }
            return retorna;
        }

        public JsonResult atendimento()
        {
            ChatController chat = new ChatController();
            bool temMensagem = chat.buscaMensagemConsultor(Session["consultor"].ToString());
            return Json(new { resultado = temMensagem });
        }
    }
}