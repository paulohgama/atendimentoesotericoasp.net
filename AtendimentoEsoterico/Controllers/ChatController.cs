﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MySql.Data.MySqlClient;
using AtendimentoEsoterico;
using System.Data;

namespace AtendimentoEsoterico.Controllers
{
    public class ChatController : Controller
    {
        BancoComandos bd = new BancoComandos();
        // GET: Chat
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Conversar(int id)
        {
            if (Request.Form.Count > 0)
            {
                string nome = Request.Form.Get("nome");
                string email = Request.Form.Get("email");
                string telefone = Request.Form.Get("telefone");
                string tempo = Request.Form.Get("tempo");
                string query = "insert into aspnet_atendimento(nomeCliente, emailCliente, telefoneCliente, tempoAtendimento, consultor_id) values (@nome, @email, @telefone, @tempo, @id)";
                MySqlParameter[] param = new MySqlParameter[]
                {
                new MySqlParameter("@nome", nome),
                new MySqlParameter("@email", email),
                new MySqlParameter("@telefone", telefone),
                new MySqlParameter("@tempo", tempo),
                new MySqlParameter("@id", id)
                };
                int linhasAfetadas = bd.Set(query, param);
                if (linhasAfetadas > 0)
                {
                    query = "select * FROM aspnet_atendimento inner join aspnet_consultor on id_consultor = consultor_id where nomeCliente = @nome and emailCliente = @email and telefoneCliente = @telefone and tempoAtendimento = @tempo and consultor_id = @id order by atendimento_id desc";
                    DataRow dr = bd.Get(query, param).Tables[0].Rows[0];
                    ViewBag.Dados = dr;
                    ViewBag.Nome = dr[7];
                    Session.Add("meuNome", dr[1]);
                    Session.Add("remetente", dr[0]);
                    Session.Add("destinatario", dr[5]);
                    return View();
                }
                else
                {
                    ViewBag.Error = "Erro ao inserir";
                    return View("FormCliente");
                }
            }
            else
            {
                string query = "select * FROM aspnet_atendimento inner join aspnet_consultor on id_consultor = consultor_id where consultor_id = " + id + " order by atendimento_id desc";
                DataRow dr = bd.Get(query, null).Tables[0].Rows[0];
                ViewBag.Dados = dr;
                ViewBag.Nome = dr[1];
                Session.Add("meuNome", dr[7]);
                Session.Add("remetente", dr[5]);
                Session.Add("destinatario", dr[0]);
                return View();
            }
        }
        public ActionResult FormCliente(int id)
        {
            ViewBag.Id = id;
            return View();
        }
        [HttpPost]
        public JsonResult salvaMensagem()
        {
            string json = "";
            try
            {
                string mensagem = Request.Form.Get("mensagem");
                string remetente = Request.Form.Get("remetente");
                string destinatario = Request.Form.Get("destinatario");
                string query = "insert into aspnet_chat (conteudo_mensagem, remetente, destinatario, lido) values (@mensagem, @remetente, @destinatario, @lido);";
                MySqlParameter[] mySqlParameter = new MySqlParameter[]
                {
                new MySqlParameter("@mensagem", mensagem),
                new MySqlParameter("@remetente", int.Parse(remetente)),
                new MySqlParameter("@destinatario", int.Parse(destinatario)),
                new MySqlParameter("@lido", 2)
                };
                int linhasAfetadas = bd.Set(query, mySqlParameter);
                if (linhasAfetadas > 0)
                {
                    json = "Enviada com sucesso";
                }
                else
                {
                    json = "Mensagem não enviada";
                }
            }
            catch(MySqlException ex)
            {
                json = ex.Message.ToString();
            }
            return Json(new { retorno = json });
        }

        public JsonResult buscaMensagem()
        {
            string remetente = Request.Form.Get("remetente");
            string destinatario = Request.Form.Get("destinatario");
            string query = "select * FROM aspnet_chat where remetente = @remetente and destinatario = @destinatario and lido = 2";
            MySqlParameter[] param = new MySqlParameter[]
            {
                new MySqlParameter("@remetente", int.Parse(remetente)),
                new MySqlParameter("@destinatario", int.Parse(destinatario))
            };
            DataTable dt = bd.Get(query, param).Tables[0];
            object[] mensagem = new object[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                object[] conteudoMensagem = new object[3];
                conteudoMensagem[0] = dt.Rows[i][1].ToString();
                conteudoMensagem[1] = dt.Rows[i][3].ToString();
                conteudoMensagem[2] = dt.Rows[i][2].ToString();
                mensagem[i] = conteudoMensagem;
            }
            for(int i = 0; i < dt.Rows.Count; i++)
            {
                query = "update aspnet_chat set lido = 3 where id_mensagem = " + dt.Rows[i][0] + ";";
                bd.Set(query, null);
            }
            return Json(mensagem);
        }

        public bool buscaMensagemConsultor(string destinatario)
        {
            string query = "select * FROM aspnet_chat where destinatario = @destinatario and lido = 2";
            MySqlParameter[] param = new MySqlParameter[]
            {
                new MySqlParameter("@destinatario", int.Parse(destinatario))
            };
            int dt = bd.Get(query, param).Tables[0].Rows.Count;

            return (dt > 0);
        }
    }
}