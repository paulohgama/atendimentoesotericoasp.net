﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MySql.Data.MySqlClient;
using AtendimentoEsoterico;
using System.IO;
using System.Data;

namespace AtendimentoEsoterico.Controllers
{
    public class AdminController : Controller
    {
        BancoComandos bd = new BancoComandos();
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddConsultor()
        {
            Session.Remove("Titulo");
            Session.Add("Titulo", "Adicionando consultor");
            Session.Remove("Exibicao");
            Session.Add("Exibicao", "Adicionando");
            Session.Remove("Tipo");
            Session.Add("Tipo", "Adicionar");
            return View();
        }
        [HttpPost]
        public ActionResult SalvarConsultor(string nome, string email, HttpPostedFileBase foto, string senha, string desc)
        {
            if (foto != null)
            {
                if (foto.FileName.EndsWith("jpg") || foto.FileName.EndsWith("jpeg") || foto.FileName.EndsWith("png") || foto.FileName.EndsWith("bmp"))
                {
                    string pathFoto = "";

                    pathFoto = Server.MapPath("~/Content/Imagens/" + email + foto.FileName);
                    if (System.IO.File.Exists(pathFoto))
                        System.IO.File.Delete(pathFoto);
                    foto.SaveAs(pathFoto);
                    pathFoto = "/Content/Imagens/" + email + foto.FileName;
                    MySqlParameter[] inserindo = new MySqlParameter[]
                    {
                    new MySqlParameter("@nome", nome),
                    new MySqlParameter("@email", email),
                    new MySqlParameter("@foto", pathFoto),
                    new MySqlParameter("@senha", senha.GetHashCode().ToString()),
                    new MySqlParameter("@desc", desc),
                    new MySqlParameter("@status", 1),
                    new MySqlParameter("@dias", (Request.Params.GetValues("dias") != null ) 
                                                ? RetornaDias(Request.Params.GetValues("dias")) 
                                                : "A definir"),
                    new MySqlParameter("@horarios",(Request.Params.Get("hora1") != "" && Request.Params.Get("hora2") != "") 
                                                    ? Request.Params.Get("hora1") + " até " + Request.Params.Get("hora2") 
                                                    : "A definir")
                    };
                    BancoComandos bd = new BancoComandos();
                    string inicio = "insert into aspnet_consultor";
                    string campos = " (nome, logotipo, email, senha, descricao, status, dias, horarios) ";
                    string values = "values (@nome, @foto, @email, @senha, @desc, @status, @dias, @horarios);";
                    int retorno = bd.Set(inicio + campos + values, inserindo);
                    return View("AddConsultor");
                }
                else
                {
                    ViewBag.ErroImagem = "Tipo de arquivo invalido";
                    return View("AddConsultor");
                }
            }
            else
            {
                ViewBag.ErroImagem = "Favor selecionar uma imagem";
                return View("AddConsultor");
            }
            
        }

        public ActionResult Consultores()
        {
            return View();
        }
        public ActionResult Editar()
        {
            int id = int.Parse(Session["id"].ToString());
            string query = "SELECT * FROM aspnet_consultor where id_consultor = @id";
            MySqlParameter[] param = new MySqlParameter[]
            {
                new MySqlParameter("@id", id)
            };
            DataSet ds = null;
            try { ds = bd.Get(query, param); } catch (MySqlException ex) { Console.Write(ex); }
            ViewBag.Dados = ds.Tables[0];
            return View("AddConsultor");
        }
        public string RetornaDias(string[] dias)
        {
            string retorno = "";
            for(int i = 0; i < dias.Length; i++)
            {
                retorno += (dias.Length-1 != i) ? dias[i] + ", " : dias[i];
            }
            return retorno;
        }
    }
}
