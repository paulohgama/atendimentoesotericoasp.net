﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AtendimentoEsoterico;
using MySql.Data.MySqlClient;
using System.Data;

namespace AtendimentoEsoterico.Controllers
{
    public class LoginController : Controller
    {
        private DataSet resultado;
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Logar(string email, string password)
        {
            BancoComandos cmd = new BancoComandos();
            MySqlParameter[] parametros = new MySqlParameter[]
            {
                new MySqlParameter("@email", email),
                new MySqlParameter("@senha",password.GetHashCode())
            };
            resultado = cmd.Get("SELECT * FROM aspnet_admin where nm_email = @email and cd_senha = @senha", parametros);
            if(resultado.Tables[0].Rows.Count == 0)
            {
                resultado = cmd.Get("SELECT * FROM aspnet_consultor where email = @email and senha = @senha", parametros);
                if (resultado.Tables[0].Rows.Count == 0)
                {
                    ViewBag.Error = "Usuario ou senha invalidos";
                    ViewBag.User = email;
                    return View("Index");
                }
                else
                {
                    Session.Add("consultor", resultado.Tables[0].Rows[0][0].ToString());
                    return RedirectToAction("Index", "Consultor");
                }

            }
            else
            {
                Session.Add("admin", resultado.Tables[0].Rows[0][0].ToString());
                return RedirectToAction("Index", "Admin");
            }

        }

        public ActionResult Logout()
        {
            Session.RemoveAll();
            return RedirectToAction("Index", "Home");
        }
    }
}