﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MySql.Data.MySqlClient;
using System.Data;
using AtendimentoEsoterico;

namespace AtendimentoEsoterico.Controllers
{
    public class HomeController : Controller
    {
        private DataSet dt;
        // GET: Home
        public ActionResult Index()
        {
            BancoComandos bd = new BancoComandos();
            dt = bd.Get("SELECT * FROM aspnet_consultor WHERE status = 1;", null);
            ViewBag.Consultores = dt;
            return View();
        }
    }
}